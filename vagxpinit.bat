::xpvag - setup xp vagrant box
@echo off
::see http://blog.syntaxc4.net/post/2014/09/03/windows-boxes-for-vagrant-courtesy-of-modern-ie.aspx
vagrant box add xp https://modernievirt.blob.core.windows.net/vhd/VMBuild_20140627/Vagrant/IE6.XP.For.Vagrant.box
vagrant init xp
vagrant up
::EOF