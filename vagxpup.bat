::xpvag - start up xp vagrant box
@echo off
:ADMINCHECK
NET SESSION >nul 2>&1
IF NOT ERRORLEVEL == 0 (
    ECHO ### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED
    ECHO See: http://support.microsoft.com/kb/305780
    ECHO.
    ECHO Press any key to exit...
    PAUSE>NUL
    EXIT /B 1
)
cd %~dp0
vagrant up
pause
::EOF