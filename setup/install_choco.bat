::install_choco - Install Chocolatey
@ECHO OFF
SET DELAY=5
:PSCHK
POWERSHELL /? >NUL 2>&1
IF NOT ERRORLEVEL == 0 (
    ECHO *** ERROR: UNABLE TO FIND POWERSHELL
    ECHO.
    ECHO Press any key to exit...
    PAUSE>NUL
    EXIT /B 1
)
:INSTALL
ECHO *** Installing...
powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))"
SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin
:END
ECHO *** Done!
FOR /L %%# IN (%DELAY%,-1,1) DO (PING -n 2 127.1>NUL & <NUL SET /P #=.)
:EOF