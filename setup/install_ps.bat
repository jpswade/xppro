::install_ps - Install PowerShell on Windows XP
::based on install_suptools by @jpswade
::@see http://blogs.technet.com/b/heyscriptingguy/archive/2011/01/28/install-powershell-on-windows-xp-and-copying-files.aspx
::@see http://support.microsoft.com/kb/968929
@ECHO OFF
:SETTINGS
SET INSTALLURL=http://download.microsoft.com/download/E/C/E/ECE99583-2003-455D-B681-68DB610B44A4/WindowsXP-KB968930-x86-ENG.exe
SET SOURCEDIR=%TEMP%
SET INSTALLNAME=PowerShell
SET DELAY=5
FOR %%F in (%INSTALLURL%) DO SET INSTALLEXE=%%~nxF
:START
TITLE %INSTALLNAME% Installer
ECHO *** %INSTALLNAME% Installer
ECHO.
:XPCHECK
VER | FIND "XP" >NUL
IF NOT ERRORLEVEL == 0 (
    ECHO ### ERROR: WINDOWS XP REQUIRED
    ECHO.
    ECHO Press any key to exit...
    PAUSE>NUL
    EXIT /B 1
)
:ADMINCHECK
NET SESSION >nul 2>&1
IF NOT ERRORLEVEL == 0 (
    ECHO *** ERROR: ADMINISTRATOR PRIVILEGES REQUIRED
    ECHO @see: http://support.microsoft.com/kb/305780
    ECHO.
    ECHO Press any key to exit...
    PAUSE>NUL
    EXIT /B 1
)
:INSTALLNOW
IF EXIST %INSTALLEXE% GOTO INSTALL
:COPY
IF EXIST "%~dp0\%INSTALLEXE%" (
  ECHO *** Copying...
  COPY /Y "%~dp0\%INSTALLEXE%" "%SOURCEDIR%\%INSTALLEXE%"
  GOTO INSTALLNOW
)
:DOWNLOAD
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" (
    ECHO *** Checking for BITSADMIN...
    BITSADMIN >NUL 2>&1
    IF NOT ERRORLEVEL 9009 (
        ECHO *** Downloading via BITSADMIN...
        BITSADMIN /CANCEL "%INSTALLEXE%">NUL
        START /WAIT BITSADMIN /TRANSFER "%INSTALLEXE%" "%INSTALLURL%" "%SOURCEDIR%\%INSTALLEXE%"
    ) ELSE (
        ECHO *** WARNING: BITSADMIN WAS NOT FOUND
    )
)
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" (
    ECHO *** ERROR: %INSTALLEXE% was not found.
    ECHO Please download %INSTALLEXE% to %SOURCEDIR% before continuing.
    START /WAIT IEXPLORE "%INSTALLURL%"
    PAUSE
)
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" GOTO DOWNLOAD
:INSTALL
ECHO *** Installing...
"%SOURCEDIR%\%INSTALLEXE%" /quiet /passive /norestart
:END
ECHO *** Done!
FOR /L %%# IN (%DELAY%,-1,1) DO (PING -n 2 127.1>NUL & <NUL SET /P #=.)
:EOF