::install_mse_xp - installs Microsoft Security Essentials
::based on install_ie8 by @jpswade
@ECHO OFF
:SETTINGS
SET SOURCEDIR=%TEMP%
SET INSTALLNAME=Microsoft Security Essentials
SET INSTALLURL=http://download.microsoft.com/download/A/3/8/A38FFBF2-1122-48B4-AF60-E44F6DC28BD8/enus/x86/mseinstall.exe
::SET INSTALLURL=http://web.archive.org/web/20140325144125/http://download.microsoft.com/download/A/3/8/A38FFBF2-1122-48B4-AF60-E44F6DC28BD8/enus/x86/mseinstall.exe
SET INSTALLPATH=%ProgramFiles%\Microsoft Security Client\
SET INSTALLRUN=msseces.exe
SET DELAY=5
FOR %%F in (%INSTALLURL%) DO SET INSTALLEXE=%%~nxF
:START
TITLE %INSTALLNAME% Installer
ECHO *** %INSTALLNAME% Installer
ECHO.
:XPCHECK
VER | FIND "XP" >NUL
IF NOT ERRORLEVEL == 0 (
    ECHO *** ERROR: WINDOWS XP REQUIRED
    ECHO.
    ECHO Press any key to exit...
    PAUSE>NUL
    EXIT /B 1
)
:ADMINCHECK
NET SESSION >nul 2>&1
IF NOT ERRORLEVEL == 0 (
    ECHO *** ERROR: ADMINISTRATOR PRIVILEGES REQUIRED
    ECHO See: http://support.microsoft.com/kb/305780
    ECHO.
    ECHO Press any key to exit...
    PAUSE>NUL
    EXIT /B 1
)
:INSTALLNOW
IF EXIST %INSTALLEXE% GOTO INSTALL
:COPY
IF EXIST "%~dp0\%INSTALLEXE%" (
  ECHO *** Copying...
  COPY /Y "%~dp0\%INSTALLEXE%" "%SOURCEDIR%\%INSTALLEXE%"
  GOTO INSTALLNOW
)
:DOWNLOAD
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" (
    ECHO *** Checking for BITSADMIN...
    BITSADMIN >NUL 2>&1
    IF NOT ERRORLEVEL 9009 (
        ECHO *** Downloading via BITSADMIN...
        BITSADMIN /CANCEL "%INSTALLEXE%">NUL
        BITSADMIN /TRANSFER "%INSTALLEXE%" "%INSTALLURL%" "%SOURCEDIR%\%INSTALLEXE%"
    ) ELSE (
        ECHO *** WARNING: BITSADMIN WAS NOT FOUND
    )
)
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" (
    ECHO *** ERROR: %INSTALLEXE% was not found.
    ECHO Please download %INSTALLEXE% to %SOURCEDIR% before continuing.
    START /WAIT IEXPLORE "%INSTALLURL%"
    PAUSE
)
:INSTALL
ECHO *** Installing...
START "%INSTALLNAME%" /WAIT /D"%SOURCEDIR%" %INSTALLEXE% /s /runwgacheck /o
:UPDATE
IF NOT EXIST "%INSTALLPATH%\%INSTALLRUN%" GOTO EOF
ECHO Updating %INSTALLNAME%...
START "%INSTALLNAME%" /D"%INSTALLPATH%" %INSTALLRUN% /update
:END
ECHO *** Done!
FOR /L %%# IN (%DELAY%,-1,1) DO (PING -n 2 127.1>NUL & <NUL SET /P #=.)
:EOF