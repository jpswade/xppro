::install_chrome - Installs Chrome
::based on install_ie8 by @jpswade
@ECHO OFF
:SETTINGS
SET SOURCEDIR=%TEMP%
::@see http://www.google.co.uk/intl/en/chrome/business/browser/admin/
SET INSTALLURL=https://dl.google.com/dl/chrome/install/googlechromestandaloneenterprise.msi
SET DELAY=5
FOR %%F in (%INSTALLURL%) DO SET INSTALLEXE=%%~nxF
:START
TITLE %INSTALLNAME% Installer
ECHO *** %INSTALLNAME% Installer
ECHO.
:ADMINCHECK
NET SESSION >nul 2>&1
IF NOT ERRORLEVEL == 0 (
    ECHO *** ERROR: ADMINISTRATOR PRIVILEGES REQUIRED
    ECHO.
    ECHO Press any key to exit...
    PAUSE>NUL
    EXIT /B 1
)
:INSTALLNOW
IF EXIST "%SOURCEDIR%\%INSTALLEXE%" GOTO INSTALL
:COPY
IF EXIST "%~dp0\%INSTALLEXE%" (
  ECHO *** Copying...
  COPY /Y "%~dp0\%INSTALLEXE%" "%SOURCEDIR%\%INSTALLEXE%"
  GOTO INSTALLNOW
)
:DOWNLOAD
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" (
    ECHO *** Checking for BITSADMIN...
    BITSADMIN >NUL 2>&1
    IF NOT ERRORLEVEL 9009 (
        ECHO *** Downloading via BITSADMIN...
        BITSADMIN /CANCEL "%INSTALLEXE%">NUL
        START /WAIT BITSADMIN /TRANSFER "%INSTALLEXE%" "%INSTALLURL%" "%SOURCEDIR%\%INSTALLEXE%"
    ) ELSE (
        ECHO *** WARNING: BITSADMIN WAS NOT FOUND
    )
)
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" (
    ECHO *** ERROR: %INSTALLEXE% was not found.
    ECHO Please download %INSTALLEXE% to %SOURCEDIR% before continuing.
    START /WAIT IEXPLORE "%INSTALLURL%"
    PAUSE
)
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" GOTO DOWNLOAD
:INSTALL
ECHO *** Installing...
MSIEXEC /i "%SOURCEDIR%\%INSTALLEXE%" /qn /quiet /norestart
:END
ECHO *** Done!
FOR /L %%# IN (%DELAY%,-1,1) DO (PING -n 2 127.1>NUL & <NUL SET /P #=.)
:EOF