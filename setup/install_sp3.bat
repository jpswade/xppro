::install_sp3 - Installs Service Pack 3 for Windows XP
::based on install_suptools by @jpswade
::@see http://www.microsoft.com/en-gb/download/details.aspx?id=24
@ECHO OFF
SET INSTALLURL=http://download.microsoft.com/download/d/3/0/d30e32d8-418a-469d-b600-f32ce3edf42d/WindowsXP-KB936929-SP3-x86-ENU.exe
SET SOURCEDIR=%TEMP%
SET TITLE=Service Pack 3
SET REGKEY="HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion"
SET REGVAL=CSDVersion
SET DELAY=5
FOR %%F in (%INSTALLURL%) DO SET INSTALLEXE=%%~nxF
:START
TITLE %TITLE% Installer
ECHO *** %TITLE% Installer
ECHO.
:XPCHECK
VER | FIND "XP" >NUL
IF NOT ERRORLEVEL == 0 (
    ECHO *** ERROR: WINDOWS XP REQUIRED
    ECHO.
    ECHO Press any key to exit...
    PAUSE>NUL
    EXIT /B 1
)
:ADMINCHECK
NET SESSION >nul 2>&1
IF NOT ERRORLEVEL == 0 (
    ECHO *** ERROR: ADMINISTRATOR PRIVILEGES REQUIRED
    ECHO @see http://support.microsoft.com/kb/305780
    ECHO.
    ECHO Press any key to exit...
    PAUSE>NUL
    EXIT /B 1
)
:CHECKVERSION
REG QUERY %REGKEY% /v "%REGVAL%" | FINDSTR "%REGVAL%" | FINDSTR "%TITLE%">NUL
IF {%ERRORLEVEL%}=={0} (
    ECHO *** ERROR: %TITLE% IS ALREADY INSTALLED!
    FOR /L %%# IN (%DELAY%,-1,1) DO (PING -n 2 127.1>NUL & <NUL SET /P #=.)
    GOTO EOF
)
:INSTALLNOW
IF EXIST %INSTALLEXE% GOTO INSTALL
:COPY
IF EXIST "%~dp0\%INSTALLEXE%" (
  ECHO *** Copying...
  COPY /Y "%~dp0\%INSTALLEXE%" "%SOURCEDIR%\%INSTALLEXE%"
  GOTO INSTALLNOW
)
:DOWNLOAD
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" (
    ECHO *** ERROR: %INSTALLEXE% WAS NOT FOUND.
    ECHO Please download %INSTALLEXE% to %SOURCEDIR% before continuing.
    START /WAIT IEXPLORE "%INSTALLURL%"
    PAUSE
)
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" GOTO DOWNLOAD
:INSTALL
ECHO *** Installing...
MSIEXEC /i "%INSTALLMSI%" /q
:END
ECHO *** Done!
FOR /L %%# IN (%DELAY%,-1,1) DO (PING -n 2 127.1>NUL & <NUL SET /P #=.)
:EOF