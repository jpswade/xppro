::install_dotnetfx.bat - installs .net
::based on install_ie8.bat by @jpswade
@ECHO OFF
:SETTINGS
SET INSTALLNAME=Microsoft .NET Framework 3.5 Service pack 1 (Full Package)
SET SOURCEDIR=%TEMP%
::SET INSTALLURL=http://www.microsoft.com/en-gb/download/details.aspx?id=25150
SET INSTALLURL=http://download.microsoft.com/download/2/0/E/20E90413-712F-438C-988E-FDAA79A8AC3D/dotnetfx35.exe
SET DELAY=5
FOR %%F in (%INSTALLURL%) DO SET INSTALLEXE=%%~nxF
:START
TITLE %INSTALLNAME% Installer
ECHO *** %INSTALLNAME% Installer
ECHO.
:ADMINCHECK
NET SESSION >nul 2>&1
IF NOT ERRORLEVEL == 0 (
    ECHO ### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED
    ECHO @see http://support.microsoft.com/kb/305780
    ECHO.
    ECHO Press any key to exit...
    PAUSE>NUL
    EXIT /B 1
)
:INSTALLNOW
IF EXIST %INSTALLEXE% GOTO INSTALL
:COPY
IF EXIST "%~dp0\%INSTALLEXE%" (
  ECHO *** Copying...
  COPY /Y "%~dp0\%INSTALLEXE%" "%SOURCEDIR%\%INSTALLEXE%"
  GOTO INSTALLNOW
)
:COPY
IF EXIST "%~dp0\%INSTALLEXE%" (
  ECHO *** Copying...
  COPY /Y "%~dp0\%INSTALLEXE%" "%SOURCEDIR%\%INSTALLEXE%"
  GOTO INSTALLNOW
)
:DOWNLOAD
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" (
    ECHO *** Checking for BITSADMIN...
    BITSADMIN >NUL 2>&1
    IF NOT ERRORLEVEL 9009 (
        ECHO *** Downloading via BITSADMIN...
        BITSADMIN /CANCEL "%INSTALLEXE%">NUL
        START /WAIT BITSADMIN /TRANSFER "%INSTALLEXE%" "%INSTALLURL%" "%SOURCEDIR%\%INSTALLEXE%"
    ) ELSE (
        ECHO *** WARNING: BITSADMIN WAS NOT FOUND
    )
)
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" (
    ECHO *** ERROR: %INSTALLEXE% was not found.
    ECHO Please download %INSTALLEXE% to %SOURCEDIR% before continuing.
    START /WAIT IEXPLORE "%INSTALLURL%"
    PAUSE
)
IF NOT EXIST "%SOURCEDIR%\%INSTALLEXE%" GOTO DOWNLOAD
:INSTALL
ECHO *** Installing...
"%SOURCEDIR%\%INSTALLEXE%" /q /norestart
:END
ECHO *** Done!
FOR /L %%# IN (%DELAY%,-1,1) DO (PING -n 2 127.1>NUL & <NUL SET /P #=.)
:EOF